<?php
/**
 * Google Map Shortcode
 */

if( !function_exists( 'movedo_ext_vce_gmap_shortcode' ) ) {

	function movedo_ext_vce_gmap_shortcode( $atts, $content ) {
		$output = $el_class = '';

		extract(
			shortcode_atts(
				array(
					'map_lat' => '51.516221',
					'map_lng' => '-0.136986',
					'map_height' => '280',
					'map_marker' => '',
					'map_zoom' => 14,
					'map_disable_style' => 'no',
					'margin_bottom' => '',
					'el_class' => '',
				),
				$atts
			)
		);

		wp_enqueue_script( 'movedo-grve-maps-script');

		$gmap_classes = array( 'grve-element', 'grve-map' );

		if ( !empty ( $el_class ) ) {
			array_push( $gmap_classes, $el_class );
		}
		$gmap_class_string = implode( ' ', $gmap_classes );

		$style = movedo_ext_vce_build_margin_bottom_style( $margin_bottom );
		if ( empty( $map_marker ) ) {
			$map_marker = get_template_directory_uri() . '/images/markers/markers.png';
		} else {
			$id = preg_replace('/[^\d]/', '', $map_marker);
			$full_src = wp_get_attachment_image_src( $id, 'full' );
			$map_marker = $full_src[0];
		}

		$map_title = '';

		$data_map = 'data-lat="' . esc_attr( $map_lat ) . '" data-lng="' . esc_attr( $map_lng ) . '" data-zoom="' . esc_attr( $map_zoom ) . '" data-disable-style="' . esc_attr( $map_disable_style ) . '"';
		$output .= '<div class="grve-map-wrapper">';
		$output .= '  <div style="display:none" class="grve-map-point" data-point-lat="' . esc_attr( $map_lat ) . '" data-point-lng="' . esc_attr( $map_lng ) . '" data-point-marker="' . esc_attr( $map_marker ) . '" data-point-title="' . esc_attr( $map_title ) . '"></div>';
		$output .= '  <div class="' . esc_attr( $gmap_class_string ) . '" ' . $data_map . ' style="' . $style . movedo_ext_vce_build_dimension( 'height', $map_height ) . '"></div>';
		$output .= '</div>';


		return $output;
	}
	add_shortcode( 'movedo_gmap', 'movedo_ext_vce_gmap_shortcode' );

}

/**
 * Add shortcode to Visual Composer
 */

if( !function_exists( 'movedo_ext_vce_gmap_shortcode_params' ) ) {
	function movedo_ext_vce_gmap_shortcode_params( $tag ) {
		return array(
			"name" => esc_html__( "Google Map", "movedo-extension" ),
			"description" => esc_html__( "Freely place your Google Map", "movedo-extension" ),
			"base" => $tag,
			"class" => "",
			"icon"      => "icon-wpb-grve-gmap",
			"category" => esc_html__( "Content", "js_composer" ),
			"params" => array(
				array(
					"type" => "textfield",
					"heading" => esc_html__( "Map Latitude", "movedo-extension" ),
					"param_name" => "map_lat",
					"value" => "51.516221",
					"description" => esc_html__( "Type map Latitude.", "movedo-extension" ),
					"admin_label" => true,
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__( "Map Longtitude", "movedo-extension" ),
					"param_name" => "map_lng",
					"value" => "-0.136986",
					"description" => esc_html__( "Type map Longtitude.", "movedo-extension" ),
					"admin_label" => true,
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__( "Map Zoom", "movedo-extension" ),
					"param_name" => "map_zoom",
					"value" => array( 1, 2, 3 ,4, 5, 6, 7, 8 ,9 ,10 ,11 ,12, 13, 14, 15, 16, 17, 18, 19 ),
					"std" => 14,
					"description" => esc_html__( "Zoom of the map.", "movedo-extension" ),
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__( "Map Height", "movedo-extension" ),
					"param_name" => "map_height",
					"value" => "280",
					"description" => esc_html__( "Type map height.", "movedo-extension" ),
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__( "Disable Custom Style", "movedo-extension" ),
					"param_name" => "map_disable_style",
					"value" => array(
						esc_html__( "No", "movedo-extension" ) => 'no',
						esc_html__( "Yes", "movedo-extension" ) => 'yes',
					),
					"description" => esc_html__( "Select if you want to disable custom map style.", "movedo-extension" ),
				),
				array(
					"type" => "attach_image",
					"heading" => esc_html__( "Custom marker", "movedo-extension" ),
					"param_name" => "map_marker",
					"value" => '',
					"description" => esc_html__( "Select an icon for custom marker.", "movedo-extension" ),
				),
				movedo_ext_vce_add_margin_bottom(),
				movedo_ext_vce_add_el_class(),
			),
		);
	}
}

if( function_exists( 'vc_lean_map' ) ) {
	vc_lean_map( 'movedo_gmap', 'movedo_ext_vce_gmap_shortcode_params' );
} else if( function_exists( 'vc_map' ) ) {
	$attributes = movedo_ext_vce_gmap_shortcode_params( 'movedo_gmap' );
	vc_map( $attributes );
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
