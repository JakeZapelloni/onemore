<?php
/**
 * Title Shortcode
 */

if( !function_exists( 'movedo_ext_vce_title_shortcode' ) ) {

	function movedo_ext_vce_title_shortcode( $atts, $content ) {

		$output = $data = $el_class = '';

		extract(
			shortcode_atts(
				array(
					'title' => '',
					'heading_tag' => 'h3',
					'heading' => 'h3',
					'custom_font_family' => '',
					'increase_heading' => '100',
					'line_type' => 'no-line',
					'line_width' => '50',
					'line_height' => '2',
					'line_color' => 'primary-1',
					'align' => 'left',
					'animation' => '',
					'animation_delay' => '200',
					'margin_bottom' => '',
					'el_class' => '',
				),
				$atts
			)
		);

		$style = movedo_ext_vce_build_margin_bottom_style( $margin_bottom );

		$title_classes = array( 'grve-element' , 'grve-title' );

		array_push( $title_classes, 'grve-align-' . $align );

		if ( !empty( $animation ) ) {
			array_push( $title_classes, 'grve-animated-item' );
			array_push( $title_classes, $animation);
			$data = ' data-delay="' . esc_attr( $animation_delay ) . '"';
		}

		if ( !empty( $heading ) ) {
			array_push( $title_classes, 'grve-' . $heading );
		}

		if ( !empty( $custom_font_family ) ) {
			array_push( $title_classes, 'grve-' . $custom_font_family );
		}

		if ( !empty( $el_class ) ) {
			array_push( $title_classes, $el_class );
		}

		if( '100' != $increase_heading ){
			array_push( $title_classes, 'grve-increase-heading' );
			array_push( $title_classes, 'grve-heading-' . $increase_heading );
		}

		$title_class_string = implode( ' ', $title_classes );

		$output .= '<' . tag_escape( $heading_tag ) . ' class="' . esc_attr( $title_class_string ) . '" style="' . $style . '"' . $data . '>';
		$output .= '<span>';
		$output .= movedo_ext_vce_auto_br( $content );
		if( 'line' == $line_type ) {
			$line_style = '';
			$line_style .= 'width: '.(preg_match('/(px|em|\%|pt|cm)$/', $line_width) ? $line_width : $line_width.'px').';';
			$line_style .= 'height: '. $line_height. 'px;';
			$output .= '<span class="grve-title-line grve-bg-' . esc_attr( $line_color ) . '" style="' . esc_attr( $line_style ) . '"></span>';
		}
		$output .= '</span>';
		$output .= '</' . tag_escape( $heading_tag ) . '>';

		return $output;
	}
	add_shortcode( 'movedo_title', 'movedo_ext_vce_title_shortcode' );

}

/**
 * Add shortcode to Visual Composer
 */

if( !function_exists( 'movedo_ext_vce_title_shortcode_params' ) ) {
	function movedo_ext_vce_title_shortcode_params( $tag ) {
		return array(
			"name" => esc_html__( "Title", "movedo-extension" ),
			"description" => esc_html__( "Add a title in many and diverse ways", "movedo-extension" ),
			"base" => $tag,
			"class" => "",
			"icon"      => "icon-wpb-grve-title",
			"category" => esc_html__( "Content", "js_composer" ),
			"params" => array(
				array(
					"type" => "textarea_html",
					"heading" => esc_html__( "Title Content", "movedo-extension" ),
					"param_name" => "content",
					"value" => "Sample Title",
					"description" => esc_html__( "Enter your title here.", "movedo-extension" ),
					"save_always" => true,
					"admin_label" => true,
				),
				movedo_ext_vce_get_heading_tag( "h3" ),
				movedo_ext_vce_get_heading( "h3" ),
				movedo_ext_vce_get_heading_increase(),
				movedo_ext_vce_get_custom_font_family(),
				array(
					"type" => "dropdown",
					"heading" => esc_html__( "Line type", "movedo-extension" ),
					"param_name" => "line_type",
					"value" => array(
						esc_html__( "No Line", "movedo-extension" ) => 'no-line',
						esc_html__( "With Line", "movedo-extension" ) => 'line',
					),
					"description" => '',
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__( "Line Width", "movedo-extension" ),
					"param_name" => "line_width",
					"value" => "50",
					"description" => esc_html__( "Enter the width for your line (Note: CSS measurement units allowed).", "movedo-extension" ),
					"dependency" => array( 'element' => "line_type", 'value' => array( 'line' ) ),
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__( "Line Height", "movedo-extension" ),
					"param_name" => "line_height",
					"value" => "2",
					"std" => "2",
					"value" => array( '1', '2', '3', '4' , '5', '6', '7', '8', '9' , '10' ),
					"description" => esc_html__( "Enter the hight for your line in px.", "movedo-extension" ),
					"dependency" => array( 'element' => "line_type", 'value' => array( 'line' ) ),
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__( "Line Color", "movedo-extension" ),
					"param_name" => "line_color",
					"value" => array(
						esc_html__( "Primary 1", "movedo-extension" ) => 'primary-1',
						esc_html__( "Primary 2", "movedo-extension" ) => 'primary-2',
						esc_html__( "Primary 3", "movedo-extension" ) => 'primary-3',
						esc_html__( "Primary 4", "movedo-extension" ) => 'primary-4',
						esc_html__( "Primary 5", "movedo-extension" ) => 'primary-5',
						esc_html__( "Primary 6", "movedo-extension" ) => 'primary-6',
						esc_html__( "Green", "movedo-extension" ) => 'green',
						esc_html__( "Orange", "movedo-extension" ) => 'orange',
						esc_html__( "Red", "movedo-extension" ) => 'red',
						esc_html__( "Blue", "movedo-extension" ) => 'blue',
						esc_html__( "Aqua", "movedo-extension" ) => 'aqua',
						esc_html__( "Purple", "movedo-extension" ) => 'purple',
						esc_html__( "Black", "movedo-extension" ) => 'black',
						esc_html__( "Grey", "movedo-extension" ) => 'grey',
						esc_html__( "White", "movedo-extension" ) => 'white',
					),
					"description" => esc_html__( "Color for the line.", "movedo-extension" ),
					"dependency" => array( 'element' => "line_type", 'value' => array( 'line' ) ),
				),
				movedo_ext_vce_add_align(),
				movedo_ext_vce_add_animation(),
				movedo_ext_vce_add_animation_delay(),
				movedo_ext_vce_add_margin_bottom(),
				movedo_ext_vce_add_el_class(),
			),
		);
	}
}

if( function_exists( 'vc_lean_map' ) ) {
	vc_lean_map( 'movedo_title', 'movedo_ext_vce_title_shortcode_params' );
} else if( function_exists( 'vc_map' ) ) {
	$attributes = movedo_ext_vce_title_shortcode_params( 'movedo_title' );
	vc_map( $attributes );
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
