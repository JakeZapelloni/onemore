<?php
/**
 * Quote Shortcode
 */

if( !function_exists( 'movedo_ext_vce_quote_shortcode' ) ) {

	function movedo_ext_vce_quote_shortcode( $atts, $content ) {

		$output = $data = $el_class = '';

		extract(
			shortcode_atts(
				array(
					'animation' => '',
					'animation_delay' => '200',
					'margin_bottom' => '',
					'align' => 'left',
					'el_class' => '',
				),
				$atts
			)
		);

		$quote_classes = array( 'grve-element' );

		if ( !empty( $animation ) ) {
			array_push( $quote_classes, 'grve-animated-item' );
			array_push( $quote_classes, $animation);
			$data = ' data-delay="' . esc_attr( $animation_delay ) . '"';
		}

		array_push( $quote_classes, 'grve-align-' . $align );

		if ( !empty( $el_class ) ) {
			array_push( $quote_classes, $el_class);
		}

		$quote_class_string = implode( ' ', $quote_classes );


		$style = movedo_ext_vce_build_margin_bottom_style( $margin_bottom );


		$output .= '<blockquote class="' . esc_attr( $quote_class_string ) . '" style="' . $style . '"' . $data . '>';
		$output .= '<p>' . $content . '</p>';
		$output .= '</blockquote>';


		return $output;
	}
	add_shortcode( 'movedo_quote', 'movedo_ext_vce_quote_shortcode' );

}

/**
 * Add shortcode to Visual Composer
 */

if( !function_exists( 'movedo_ext_vce_quote_shortcode_params' ) ) {
	function movedo_ext_vce_quote_shortcode_params( $tag ) {
		return array(
			"name" => esc_html__( "Quote", "movedo-extension" ),
			"description" => esc_html__( "Easily create your Quote Text", "movedo-extension" ),
			"base" => $tag,
			"class" => "",
			"icon"      => "icon-wpb-grve-quote",
			"category" => esc_html__( "Content", "js_composer" ),
			"params" => array(
				array(
					"type" => "textarea",
					"heading" => esc_html__( "Text", "movedo-extension" ),
					"param_name" => "content",
					"value" => "Sample Quote",
					"description" => esc_html__( "Type your quote.", "movedo-extension" ),
				),
				movedo_ext_vce_add_align(),
				movedo_ext_vce_add_animation(),
				movedo_ext_vce_add_animation_delay(),
				movedo_ext_vce_add_margin_bottom(),
				movedo_ext_vce_add_el_class(),
			),
		);
	}
}

if( function_exists( 'vc_lean_map' ) ) {
	vc_lean_map( 'movedo_quote', 'movedo_ext_vce_quote_shortcode_params' );
} else if( function_exists( 'vc_map' ) ) {
	$attributes = movedo_ext_vce_quote_shortcode_params( 'movedo_quote' );
	vc_map( $attributes );
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
