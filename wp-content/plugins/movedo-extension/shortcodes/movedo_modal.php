<?php
/**
 * Modal Shortcode
 */

if( !function_exists( 'movedo_ext_vce_modal_shortcode' ) ) {

	function movedo_ext_vce_modal_shortcode( $attr, $content ) {

		$output = $el_class = '';

		extract(
			shortcode_atts(
				array(
					'modal_id' => '',
					'modal_mode' => 'magnific',
					'el_class' => '',
				),
				$attr
			)
		);

		ob_start();

		?>
			<div id="<?php echo esc_attr( $modal_id ); ?>" class="grve-modal-element mfp-hide <?php echo esc_attr( $el_class ); ?>">
				<div class="grve-section">
					<div class="grve-container">
						<div class="grve-row grve-columns-gap-30">
							<div class="grve-column grve-column-1">
							<?php
								if ( !empty( $content ) ) {
									echo do_shortcode( $content );
								}
							?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
		return ob_get_clean();

	}
	add_shortcode( 'movedo_modal', 'movedo_ext_vce_modal_shortcode' );

}

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_movedo_modal extends WPBakeryShortCodesContainer {
    }
}

/**
 * Add shortcode to Visual Composer
 */

if( !function_exists( 'movedo_ext_vce_modal_shortcode_params' ) ) {
	function movedo_ext_vce_modal_shortcode_params( $tag ) {
		return array(
			"name" => esc_html__( "Modal", "movedo-extension" ),
			"description" => esc_html__( "Add a modal with elements", "movedo-extension" ),
			"base" => $tag,
			"class" => "",
			"icon" => "icon-wpb-grve-modal",
			"category" => esc_html__( "Content", "js_composer" ),
			"content_element" => true,
			"controls" => "full",
			"show_settings_on_create" => true,
			"as_parent" => array('except' => 'vc_tta_section,movedo_modal'),
			"params" => array(
				array(
					"type" => "textfield",
					"heading" => esc_html__( "Modal ID", "movedo-extension" ),
					"param_name" => "modal_id",
					"admin_label" => true,
					"description" => esc_html__( "Enter a unique id to trigger the modal from a link or button. In your link use class: grve-modal-popup and href: # following the Modal ID.", "movedo-extension" ),
				),
				movedo_ext_vce_add_el_class(),
			),
			"js_view" => 'VcColumnView',
		);
	}
}

if( function_exists( 'vc_lean_map' ) ) {
	vc_lean_map( 'movedo_modal', 'movedo_ext_vce_modal_shortcode_params' );
} else if( function_exists( 'vc_map' ) ) {
	$attributes = movedo_ext_vce_modal_shortcode_params( 'movedo_modal' );
	vc_map( $attributes );
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
