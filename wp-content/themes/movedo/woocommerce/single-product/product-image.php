<?php
/**
 * Single Product Image
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.6.3
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $woocommerce, $product;

$grve_product_image_effect = movedo_grve_option( 'product_image_effect', 'none' );

//Classes Images
$product_images_classes = array( 'images' );
if ( 'yes' != get_option( 'woocommerce_enable_lightbox', '' ) && 'popup' == movedo_grve_option( 'product_gallery_mode', 'popup' ) ) {
	$product_images_classes[] = 'grve-gallery-popup';
}
$product_images_class_string = implode( ' ', $product_images_classes );

//Classes Product Image
$product_image_classes = array( 'grve-product-image' );
if ( 'zoom' == $grve_product_image_effect ) {
	$product_image_classes[] = 'easyzoom';
} elseif( 'parallax' == $grve_product_image_effect ) {
	$product_image_classes[] = 'grve-product-parallax-image';
}
$product_image_class_string = implode( ' ', $product_image_classes );

?>
<div id="grve-product-feature-image" class="<?php echo esc_attr( $product_images_class_string ); ?>">
	<div class="<?php echo esc_attr( $product_image_class_string ); ?>">
		<?php
			if ( has_post_thumbnail() ) {

				$custom_image_id = movedo_grve_post_meta( '_movedo_grve_area_image_id' );
				if( $custom_image_id ) {
					$image_title 	= esc_attr( get_the_title( $custom_image_id  ) );
					$image_caption 	= get_post( $custom_image_id  )->post_excerpt;
					$image_link  	= wp_get_attachment_url( $custom_image_id  );

					$image       	= wp_get_attachment_image( $custom_image_id , apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), "", array(
						'alt'	=> $image_title
						) );
				} else {

					$image_title 	= esc_attr( get_the_title( get_post_thumbnail_id() ) );
					$image_caption 	= get_post( get_post_thumbnail_id() )->post_excerpt;
					$image_link  	= wp_get_attachment_url( get_post_thumbnail_id() );

					$image       	= get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
						'alt'	=> $image_title
						) );
				}

				$attachment_count = count( $product->get_gallery_attachment_ids() );

				if ( $attachment_count > 0 ) {
					$gallery = '[product-gallery]';
				} else {
					$gallery = '';
				}

				echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<a href="%s" itemprop="image" class="woocommerce-main-image zoom" title="%s" data-rel="prettyPhoto' . $gallery . '">%s</a>', $image_link, $image_caption, $image ), $post->ID );

			} else {

				echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), esc_html__( 'Placeholder', 'woocommerce' ) ), $post->ID );

			}
		?>
	</div>
	<?php do_action( 'woocommerce_product_thumbnails' ); ?>

</div>
