<?php

/*
*	Global Parameter and functions
*
* 	@version	1.0
* 	@author		Greatives Team
* 	@URI		http://greatives.eu
*/

$movedo_grve_social_list = array(
	'twitter' => 'Twitter',
	'facebook' => 'Facebook',
	'google-plus' => 'Google Plus',
	'instagram' => 'instagram',
	'linkedin' => 'LinkedIn',
	'tumblr' => 'Tumblr',
	'pinterest' => 'Pinterest',
	'github' => 'Github',
	'dribbble' => 'Dribbble',
	'reddit' => 'reddit',
	'flickr' => 'Flickr',
	'skype' => 'Skype',
	'youtube' => 'YouTube',
	'vimeo' => 'Vimeo',
	'soundcloud' => 'SoundCloud',
	'wechat' => 'WeChat',
	'weibo' => 'Weibo',
	'renren' => 'Renren',
	'qq' => 'QQ',
	'xing' => 'XING',
	'rss' => 'RSS',
	'vk' => 'VK',
	'behance' => 'Behance',
	'foursquare' => 'Foursquare',
	'steam' => 'Steam',
	'twitch' => 'Twitch',
	'houzz' => 'Houzz',
	'yelp' => 'Yelp',
);

$movedo_grve_social_list_extended = array (
	array(
		'title' => 'Twitter',
		'url' => 'twitter_url',
		'id' => 'twitter',
		'class' => 'fa fa-twitter',
	),
	array(
		'title' => 'Facebook',
		'url' => 'facebook_url',
		'id' => 'facebook',
		'class' => 'fa fa-facebook',
	),
	array(
		'title' => 'Google+',
		'url' => 'googleplus_url',
		'id' => 'google-plus',
		'class' => 'fa fa-google-plus',
	),
	array(
		'title' => 'instagram',
		'url' => 'instagram_url',
		'id' => 'instagram',
		'class' => 'fa fa-instagram',
	),
	array(
		'title' => 'LinkedIn',
		'url' => 'linkedin_url',
		'id' => 'linkedin',
		'class' => 'fa fa-linkedin',
	),
	array(
		'title' => 'Tumblr',
		'url' => 'tumblr_url',
		'id' => 'tumblr',
		'class' => 'fa fa-tumblr',
	),
	array(
		'title' => 'Pinterest',
		'url' => 'pinterest_url',
		'id' => 'pinterest',
		'class' => 'fa fa-pinterest',
	),
	array(
		'title' => 'GitHub',
		'url' => 'github_url',
		'id' => 'github',
		'class' => 'fa fa-github',
	),
	array(
		'title' => 'Dribbble',
		'url' => 'dribbble_url',
		'id' => 'dribbble',
		'class' => 'fa fa-dribbble',
	),
	array(
		'title' => 'reddit',
		'url' => 'reddit_url',
		'id' => 'reddit',
		'class' => 'fa fa-reddit',
	),
	array(
		'title' => 'Flickr',
		'url' => 'flickr_url',
		'id' => 'flickr',
		'class' => 'fa fa-flickr',
	),
	array(
		'title' => 'Skype',
		'url' => 'skype_url',
		'id' => 'skype',
		'class' => 'fa fa-skype',
	),
	array(
		'title' => 'YouTube',
		'url' => 'youtube_url',
		'id' => 'youtube',
		'class' => 'fa fa-youtube',
	),
	array(
		'title' => 'Vimeo',
		'url' => 'vimeo_url',
		'id' => 'vimeo',
		'class' => 'fa fa-vimeo',
	),
	array(
		'title' => 'SoundCloud',
		'url' => 'soundcloud_url',
		'id' => 'soundcloud',
		'class' => 'fa fa-soundcloud',
	),
	array(
		'title' => 'WeChat',
		'url' => 'wechat_url',
		'id' => 'wechat',
		'class' => 'fa fa-wechat',
	),
	array(
		'title' => 'Weibo',
		'url' => 'weibo_url',
		'id' => 'weibo',
		'class' => 'fa fa-weibo',
	),
	array(
		'title' => 'Renren',
		'url' => 'renren_url',
		'id' => 'renren',
		'class' => 'fa fa-renren',
	),
	array(
		'title' => 'QQ',
		'url' => 'qq_url',
		'id' => 'qq',
		'class' => 'fa fa-qq',
	),
	array(
		'title' => 'XING',
		'url' => 'xing_url',
		'id' => 'xing',
		'class' => 'fa fa-xing',
	),
	array(
		'title' => 'RSS',
		'url' => 'rss_url',
		'id' => 'rss',
		'class' => 'fa fa-rss',
	),
	array(
		'title' => 'VK',
		'url' => 'vk_url',
		'id' => 'vk',
		'class' => 'fa fa-vk',
	),
	array(
		'title' => 'Behance',
		'url' => 'behance_url',
		'id' => 'behance',
		'class' => 'fa fa-behance',
	),
	array(
		'title' => 'Foursquare',
		'url' => 'foursquare_url',
		'id' => 'foursquare',
		'class' => 'fa fa-foursquare',
	),
	array(
		'title' => 'Steam',
		'url' => 'steam_url',
		'id' => 'steam',
		'class' => 'fa fa-steam',
	),
	array(
		'title' => 'Twitch',
		'url' => 'twitch_url',
		'id' => 'twitch',
		'class' => 'fa fa-twitch',
	),
	array(
		'title' => 'Houzz',
		'url' => 'houzz_url',
		'id' => 'houzz',
		'class' => 'fa fa-houzz',
	),
	array(
		'title' => 'Yelp',
		'url' => 'yelp_url',
		'id' => 'yelp',
		'class' => 'fa fa-yelp',
	),
);

$movedo_grve_post_color_selection = array(
	'primary-1' => esc_html__( 'Primary 1', 'movedo' ),
	'primary-2' => esc_html__( 'Primary 2', 'movedo' ),
	'primary-3' => esc_html__( 'Primary 3', 'movedo' ),
	'primary-4' => esc_html__( 'Primary 4', 'movedo' ),
	'primary-5' => esc_html__( 'Primary 5', 'movedo' ),
	'primary-6' => esc_html__( 'Primary 6', 'movedo' ),
	'green' => esc_html__( 'Green', 'movedo' ),
	'orange' => esc_html__( 'Orange', 'movedo' ),
	'red' => esc_html__( 'Red', 'movedo' ),
	'blue' => esc_html__( 'Blue', 'movedo' ),
	'aqua' => esc_html__( 'Aqua', 'movedo' ),
	'purple' => esc_html__( 'Purple', 'movedo' ),
	'black' => esc_html__( 'Black', 'movedo' ),
	'grey' => esc_html__( 'Grey', 'movedo' ),
);

$movedo_grve_button_color_selection = array(
	'primary-1' => esc_html__( 'Primary 1', 'movedo' ),
	'primary-2' => esc_html__( 'Primary 2', 'movedo' ),
	'primary-3' => esc_html__( 'Primary 3', 'movedo' ),
	'primary-4' => esc_html__( 'Primary 4', 'movedo' ),
	'primary-5' => esc_html__( 'Primary 5', 'movedo' ),
	'primary-6' => esc_html__( 'Primary 6', 'movedo' ),
	'green' => esc_html__( 'Green', 'movedo' ),
	'orange' => esc_html__( 'Orange', 'movedo' ),
	'red' => esc_html__( 'Red', 'movedo' ),
	'blue' => esc_html__( 'Blue', 'movedo' ),
	'aqua' => esc_html__( 'Aqua', 'movedo' ),
	'purple' => esc_html__( 'Purple', 'movedo' ),
	'black' => esc_html__( 'Black', 'movedo' ),
	'grey' => esc_html__( 'Grey', 'movedo' ),
	'white' => esc_html__( 'White', 'movedo' ),
);

$movedo_grve_button_size_selection = array(
	'extrasmall' => esc_html__( 'Extra Small', 'movedo' ),
	'small' => esc_html__( 'Small', 'movedo' ),
	'medium' => esc_html__( 'Medium', 'movedo' ),
	'large' => esc_html__( 'Large', 'movedo' ),
	'extralarge' => esc_html__( 'Extra Large', 'movedo' ),
);

$movedo_grve_button_shape_selection = array(
	'square' => esc_html__( 'Square', 'movedo' ),
	'round' => esc_html__( 'Round', 'movedo' ),
	'extra-round' => esc_html__( 'Extra Round', 'movedo' ),
);

$movedo_grve_button_type_selection = array(
	'simple' => esc_html__( 'Simple', 'movedo' ),
	'outline' => esc_html__( 'Outline', 'movedo' ),
);

$movedo_grve_post_bg_opacity_selection = array(
	'none'  => '0%',
	'10'  => '10%',
	'20'  => '20%',
	'30'  => '30%',
	'40'  => '40%',
	'50'  => '50%',
	'60'  => '60%',
	'70'  => '70%',
	'80'  => '80%',
	'90'  => '90%',
	'100'  => '100%',
);

$movedo_grve_area_height = array(
	'small' => esc_html__( 'Small Height', 'movedo' ),
	'medium' => esc_html__( 'Medium Height', 'movedo' ),
	'large' => esc_html__( 'Large Height', 'movedo' ),
	'10'  => '10%',
	'15' => '15%',
	'20'  => '20%',
	'25' => '25%',
	'30'  => '30%',
	'35' => '35%',
	'40'  => '40%',
	'45' => '45%',
	'50'  => '50%',
	'55' => '55%',
	'60'  => '60%',
	'65' => '65%',
	'70'  => '70%',
	'75' => '75%',
	'80'  => '80%',
	'85'  => '85%',
	'90'  => '90%',
);

$movedo_grve_awsome_fonts_list = array( "500px", "adjust", "adn", "align-center", "align-justify", "align-left", "align-right", "amazon", "ambulance", "american-sign-language-interpreting", "anchor", "android", "angellist", "angle-double-down", "angle-double-left", "angle-double-right", "angle-double-up", "angle-down", "angle-left", "angle-right", "angle-up", "apple", "archive", "area-chart", "arrow-circle-down", "arrow-circle-left", "arrow-circle-o-down", "arrow-circle-o-left", "arrow-circle-o-right", "arrow-circle-o-up", "arrow-circle-right", "arrow-circle-up", "arrow-down", "arrow-left", "arrow-right", "arrows", "arrows-alt", "arrows-h", "arrows-v", "arrow-up", "asl-interpreting", "assistive-listening-systems", "asterisk", "at", "audio-description", "automobile", "backward", "balance-scale", "ban", "bank", "bar-chart", "bar-chart-o", "barcode", "bars", "battery-0", "battery-1", "battery-2", "battery-3", "battery-4", "battery-empty", "battery-full", "battery-half", "battery-quarter", "battery-three-quarters", "bed", "beer", "behance", "behance-square", "bell", "bell-o", "bell-slash", "bell-slash-o", "bicycle", "binoculars", "birthday-cake", "bitbucket", "bitbucket-square", "bitcoin", "black-tie", "blind", "bluetooth", "bluetooth-b", "bold", "bolt", "bomb", "book", "bookmark", "bookmark-o", "braille", "briefcase", "btc", "bug", "building", "building-o", "bullhorn", "bullseye", "bus", "buysellads", "cab", "calculator", "calendar", "calendar-check-o", "calendar-minus-o", "calendar-o", "calendar-plus-o", "calendar-times-o", "camera", "camera-retro", "car", "caret-down", "caret-left", "caret-right", "caret-square-o-down", "caret-square-o-left", "caret-square-o-right", "caret-square-o-up", "caret-up", "cart-arrow-down", "cart-plus", "cc", "cc-amex", "cc-diners-club", "cc-discover", "cc-jcb", "cc-mastercard", "cc-paypal", "cc-stripe", "cc-visa", "certificate", "chain", "chain-broken", "check", "check-circle", "check-circle-o", "check-square", "check-square-o", "chevron-circle-down", "chevron-circle-left", "chevron-circle-right", "chevron-circle-up", "chevron-down", "chevron-left", "chevron-right", "chevron-up", "child", "chrome", "circle", "circle-o", "circle-o-notch", "circle-thin", "clipboard", "clock-o", "clone", "close", "cloud", "cloud-download", "cloud-upload", "cny", "code", "code-fork", "codepen", "codiepie", "coffee", "cog", "cogs", "columns", "comment", "commenting", "commenting-o", "comment-o", "comments", "comments-o", "compass", "compress", "connectdevelop", "contao", "copy", "copyright", "creative-commons", "credit-card", "credit-card-alt", "crop", "crosshairs", "css3", "cube", "cubes", "cut", "cutlery", "dashboard", "dashcube", "database", "deaf", "deafness", "dedent", "delicious", "desktop", "deviantart", "diamond", "digg", "dollar", "dot-circle-o", "download", "dribbble", "dropbox", "drupal", "edge", "edit", "eject", "ellipsis-h", "ellipsis-v", "empire", "envelope", "envelope-o", "envelope-square", "envira", "eraser", "eur", "euro", "exchange", "exclamation", "exclamation-circle", "exclamation-triangle", "expand", "expeditedssl", "external-link", "external-link-square", "eye", "eyedropper", "eye-slash", "fa", "facebook", "facebook-f", "facebook-official", "facebook-square", "fast-backward", "fast-forward", "fax", "feed", "female", "fighter-jet", "file", "file-archive-o", "file-audio-o", "file-code-o", "file-excel-o", "file-image-o", "file-movie-o", "file-o", "file-pdf-o", "file-photo-o", "file-picture-o", "file-powerpoint-o", "files-o", "file-sound-o", "file-text", "file-text-o", "file-video-o", "file-word-o", "file-zip-o", "film", "filter", "fire", "fire-extinguisher", "firefox", "first-order", "flag", "flag-checkered", "flag-o", "flash", "flask", "flickr", "floppy-o", "folder", "folder-o", "folder-open", "folder-open-o", "font", "font-awesome", "fonticons", "fort-awesome", "forumbee", "forward", "foursquare", "frown-o", "futbol-o", "gamepad", "gavel", "gbp", "ge", "gear", "gears", "genderless", "get-pocket", "gg", "gg-circle", "gift", "git", "github", "github-alt", "github-square", "gitlab", "git-square", "gittip", "glass", "glide", "glide-g", "globe", "google", "google-plus", "google-plus-circle", "google-plus-official", "google-plus-square", "google-wallet", "graduation-cap", "gratipay", "group", "hacker-news", "hand-grab-o", "hand-lizard-o", "hand-o-down", "hand-o-left", "hand-o-right", "hand-o-up", "hand-paper-o", "hand-peace-o", "hand-pointer-o", "hand-rock-o", "hand-scissors-o", "hand-spock-o", "hand-stop-o", "hard-of-hearing", "hashtag", "hdd-o", "header", "headphones", "heart", "heartbeat", "heart-o", "history", "home", "hospital-o", "hotel", "hourglass", "hourglass-1", "hourglass-2", "hourglass-3", "hourglass-end", "hourglass-half", "hourglass-o", "hourglass-start", "houzz", "h-square", "html5", "i-cursor", "ils", "image", "inbox", "indent", "industry", "info", "info-circle", "inr", "instagram", "institution", "internet-explorer", "intersex", "ioxhost", "italic", "joomla", "jpy", "jsfiddle", "key", "keyboard-o", "krw", "language", "laptop", "lastfm", "lastfm-square", "leaf", "leanpub", "legal", "lemon-o", "level-down", "level-up", "life-bouy", "life-buoy", "life-ring", "life-saver", "lightbulb-o", "line-chart", "link", "linkedin", "linkedin-square", "linux", "list", "list-alt", "list-ol", "list-ul", "location-arrow", "lock", "long-arrow-down", "long-arrow-left", "long-arrow-right", "long-arrow-up", "low-vision", "magic", "magnet", "mail-forward", "mail-reply", "mail-reply-all", "male", "map", "map-marker", "map-o", "map-pin", "map-signs", "mars", "mars-double", "mars-stroke", "mars-stroke-h", "mars-stroke-v", "maxcdn", "meanpath", "medium", "medkit", "meh-o", "mercury", "microphone", "microphone-slash", "minus", "minus-circle", "minus-square", "minus-square-o", "mixcloud", "mobile", "mobile-phone", "modx", "money", "moon-o", "mortar-board", "motorcycle", "mouse-pointer", "music", "navicon", "neuter", "newspaper-o", "object-group", "object-ungroup", "odnoklassniki", "odnoklassniki-square", "opencart", "openid", "opera", "optin-monster", "outdent", "pagelines", "paint-brush", "paperclip", "paper-plane", "paper-plane-o", "paragraph", "paste", "pause", "pause-circle", "pause-circle-o", "paw", "paypal", "pencil", "pencil-square", "pencil-square-o", "percent", "phone", "phone-square", "photo", "picture-o", "pie-chart", "pied-piper", "pied-piper-alt", "pied-piper-pp", "pinterest", "pinterest-p", "pinterest-square", "plane", "play", "play-circle", "play-circle-o", "plug", "plus", "plus-circle", "plus-square", "plus-square-o", "power-off", "print", "product-hunt", "puzzle-piece", "qq", "qrcode", "question", "question-circle", "question-circle-o", "quote-left", "quote-right", "ra", "random", "rebel", "recycle", "reddit", "reddit-alien", "reddit-square", "refresh", "registered", "remove", "renren", "reorder", "repeat", "reply", "reply-all", "resistance", "retweet", "rmb", "road", "rocket", "rotate-left", "rotate-right", "rouble", "rss", "rss-square", "rub", "ruble", "rupee", "safari", "save", "scissors", "scribd", "search", "search-minus", "search-plus", "sellsy", "send", "send-o", "server", "share", "share-alt", "share-alt-square", "share-square", "share-square-o", "shekel", "sheqel", "shield", "ship", "shirtsinbulk", "shopping-bag", "shopping-basket", "shopping-cart", "signal", "sign-in", "signing", "sign-language", "sign-out", "simplybuilt", "sitemap", "skyatlas", "skype", "slack", "sliders", "slideshare", "smile-o", "snapchat", "snapchat-ghost", "snapchat-square", "soccer-ball-o", "sort", "sort-alpha-asc", "sort-alpha-desc", "sort-amount-asc", "sort-amount-desc", "sort-asc", "sort-desc", "sort-down", "sort-numeric-asc", "sort-numeric-desc", "sort-up", "soundcloud", "space-shuttle", "spinner", "spoon", "spotify", "square", "square-o", "stack-exchange", "stack-overflow", "star", "star-half", "star-half-empty", "star-half-full", "star-half-o", "star-o", "steam", "steam-square", "step-backward", "step-forward", "stethoscope", "sticky-note", "sticky-note-o", "stop", "stop-circle", "stop-circle-o", "street-view", "strikethrough", "stumbleupon", "stumbleupon-circle", "subscript", "subway", "suitcase", "sun-o", "superscript", "support", "table", "tablet", "tachometer", "tag", "tags", "tasks", "taxi", "television", "tencent-weibo", "terminal", "text-height", "text-width", "th", "themeisle", "th-large", "th-list", "thumbs-down", "thumbs-o-down", "thumbs-o-up", "thumbs-up", "thumb-tack", "ticket", "times", "times-circle", "times-circle-o", "tint", "toggle-down", "toggle-left", "toggle-off", "toggle-on", "toggle-right", "toggle-up", "trademark", "train", "transgender", "transgender-alt", "trash", "trash-o", "tree", "trello", "tripadvisor", "trophy", "truck", "try", "tty", "tumblr", "tumblr-square", "turkish-lira", "tv", "twitch", "twitter", "twitter-square", "umbrella", "underline", "undo", "universal-access", "university", "unlink", "unlock", "unlock-alt", "unsorted", "upload", "usb", "usd", "user", "user-md", "user-plus", "users", "user-secret", "user-times", "venus", "venus-double", "venus-mars", "viacoin", "viadeo", "viadeo-square", "video-camera", "vimeo", "vimeo-square", "vine", "vk", "volume-control-phone", "volume-down", "volume-off", "volume-up", "warning", "wechat", "weibo", "weixin", "whatsapp", "wheelchair", "wheelchair-alt", "wifi", "wikipedia-w", "windows", "won", "wordpress", "wpbeginner", "wpforms", "wrench", "xing", "xing-square", "yahoo", "yc", "y-combinator", "y-combinator-square", "yc-square", "yelp", "yen", "yoast", "youtube", "youtube-play", "youtube-square" );

/**
 * Check if Plugin is active
 */
function movedo_grve_is_plugin_active( $plugin ) {

	if ( ! function_exists( 'is_plugin_active' ) ) {
		require_once ABSPATH . 'wp-admin/includes/plugin.php';
	}
	return is_plugin_active( $plugin );

}

/**
 * Get CSS Color
 */
function movedo_grve_get_css_color( $prefix, $color ) {
	$rgb_color = preg_match( '/rgba/', $color ) ? preg_replace( array( '/\s+/', '/^rgba\((\d+)\,(\d+)\,(\d+)\,([\d\.]+)\)$/' ), array( '', 'rgb($1,$2,$3)' ), $color ) : $color;
	$string = $prefix . ':' . $rgb_color . ';';
	if ( $rgb_color !== $color ) $string .= $prefix . ':' . $color . ';';
	return $string;
}

/**
 * Get allowed HTML for microdata
 */
function movedo_grve_get_microdata_allowed_html() {
	$movedo_grve_microdata_allowed_html = array(
		'span' => array(
			'title' => true,
			'class' => true,
			'id' => true,
			'dir' => true,
			'align' => true,
			'lang' => true,
			'xml:lang' => true,
			'aria-hidden' => true,
			'data-icon' => true,
			'itemref' => true,
			'itemid' => true,
			'itemprop' => true,
			'itemscope' => true,
			'itemtype' => true,
			'xmlns:v' => true,
			'typeof' => true,
			'property' => true
		),
		'br' => array(),
	);

	return $movedo_grve_microdata_allowed_html;
}

/**
 * Get instagram array
 */
function movedo_grve_get_instagram_array( $username, $limit, $order_by, $order, $cache ) {

	$username = strtolower( $username );

	if ( false === ( $instagram = get_transient('grve-instagram-feed-v1-'.sanitize_title_with_dashes( $username ) ) ) || empty( $cache ) ) {

		$url = 'https://www.instagram.com/' . $username . '/media/';

		$remote = wp_remote_get( $url );

		if ( is_wp_error( $remote ) ) {
			if( current_user_can( 'administrator' ) ) {
				return new WP_Error( 'site_down', esc_html__( 'Unable to communicate with Instagram!', 'movedo' ) );
			} else {
				return new WP_Error('site_down', '' );
			}
		}
		if ( 200 != wp_remote_retrieve_response_code( $remote ) ) {
			if( current_user_can( 'administrator' ) ) {
				return new WP_Error('invalid_response', esc_html__( 'Instagram invalid response!', 'movedo' ) );
			} else {
				return new WP_Error('invalid_response', '' );
			}
		}

		$insta_array = json_decode( $remote['body'], TRUE );

		$images = isset( $insta_array['items'] ) ? $insta_array['items'] : array();

		$instagram = array();

		foreach ( $images as $image ) {

			if ($image['user']['username'] == $username) {

				$image['link']                          = preg_replace( "/^http:/i", "", $image['link'] );
				$image['images']['thumbnail']           = preg_replace( "/^http:/i", "", $image['images']['thumbnail'] );
				$image['images']['low_resolution']      = preg_replace( "/^http:/i", "", $image['images']['low_resolution'] );
				$image['images']['standard_resolution'] = preg_replace( "/^http:/i", "", $image['images']['standard_resolution'] );

				$instagram[] = array(
					'description'   => $image['caption']['text'],
					'link'          => $image['link'],
					'time'          => $image['created_time'],
					'comments'      => $image['comments']['count'],
					'likes'         => $image['likes']['count'],
					'thumbnail'     => $image['images']['thumbnail'],
					'medium'        => $image['images']['low_resolution'],
					'large'         => $image['images']['standard_resolution'],
					'type'          => $image['type']
				);
			}
		}

		//Instagram Order
		if ( 'none' != $order_by ) {
			foreach ($instagram as $key => $row) {
				$time[$key] = $row['time'];
				$comments[$key]  = $row['comments'];
				$likes[$key] = $row['likes'];
			}
			if ( 'ASC' == $order ) {
				$order = SORT_ASC;
			} else {
				$order = SORT_DESC;
			}
			if ( 'datetime' == $order_by ) {
				$order_by = $time;
			} elseif ( 'comments' == $order_by ) {
				$order_by = $comments;
			} elseif ( 'likes' == $order_by ) {
				$order_by = $likes;
			}
			array_multisort( $order_by, $order, $instagram );
		}

		if( !empty( $cache ) ) {
			set_transient('grve-instagram-feed-v1-'.sanitize_title_with_dashes( $username ), $instagram, apply_filters( 'movedo_grve_instagram_cache_time', HOUR_IN_SECONDS ) );
		}
	}

	return array_slice( $instagram, 0, $limit );
}

/**
 * Get Adaptive URL
 */
function movedo_grve_get_adaptive_url( $media_id, $image_size = "responsive" ) {

	if ( 'full' == $image_size ) {
		$default_src = wp_get_attachment_image_src( $media_id, 'full'  );
		$img_url = $default_src[0];
	} elseif ( 'extra-extra-large' == $image_size ) {
		$default_src = wp_get_attachment_image_src( $media_id, 'movedo-grve-fullscreen' );
		$img_url = $default_src[0];
	} else {
		$resolutions   = array( 2560, 1600, 768 );
		$hidpi         = TRUE;
		$resolution = FALSE;

		// Get resolution cookie
		if ( isset( $_GET['resolution'] ) ) {
			$cookie_resolution = $_GET['resolution'];
		} else if ( isset( $_COOKIE['resolution'] ) ) {
			$cookie_resolution = $_COOKIE['resolution'];
		} else {
			$cookie_resolution = null;
		}

		// Default values
		$client_width  = 1920;
		$pixel_density = 1;

		if ( isset( $cookie_resolution ) && preg_match( "/^[0-9]+[,]+[0-9.]+$/", $cookie_resolution ) ) {
			$cookie_array = explode( ',', $cookie_resolution );
			// Get screen width.
			if ( count( $cookie_array ) > 0 ) {
				$client_width  = intval( $cookie_array[0] );
			}
			// Get pixel density.
			if ( $hidpi ) {
				if ( count( $cookie_array ) > 1 ) {
					$pixel_density = $cookie_array[1];
				}
			}
		}

		// Scale client screen width according to its pixel density.
		$client_width_scaled = $client_width * $pixel_density;

		// Find the closest available resolution
		$closest = null;
		foreach ( $resolutions as $res ) {
			if ( $closest == null || abs( $client_width_scaled - $closest ) > abs( $res - $client_width_scaled ) ) {
				$closest = $res;
			}
		}
		$resolution = $closest;

		$default_src = wp_get_attachment_image_src( $media_id, 'movedo-grve-fullscreen' );
		$skip_check = false;
		switch( $resolution ) {
			case '2560':
				$img_src = wp_get_attachment_image_src( $media_id, 'full' );
				$skip_check = true;
			break;
			case '768':
				$img_src = wp_get_attachment_image_src( $media_id, 'large' );
			break;
			case '1600':
			default:
				$img_src = $default_src;
			break;
		}

		if( $img_src[1] > $default_src[1]  && !$skip_check ) {
			$img_url = $default_src[0];
		} else {
			$img_url = $img_src[0];
		}
	}

	return $img_url;
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
