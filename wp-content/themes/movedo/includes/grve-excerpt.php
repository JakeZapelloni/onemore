<?php

/*
 *	Excerpt functions
 *
 * 	@version	1.0
 * 	@author		Greatives Team
 * 	@URI		http://greatives.eu
 */


 /**
 * Custom excerpt
 */
function movedo_grve_excerpt( $limit, $more = '0' ) {
	global $post;
	$post_id = $post->ID;

	if ( has_excerpt( $post_id ) ) {
		$excerpt = $post->post_excerpt;
		$excerpt = apply_filters('the_excerpt', $excerpt);
		if ( '1' == $more ) {
			$excerpt .= movedo_grve_read_more();
		}
	} else {
		$content = get_the_content('');
		$content = do_shortcode( $content );
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]>', $content);
		if ( '1' == $more ) {
			$excerpt = '<p>' . wp_trim_words( $content, $limit ) . '</p>';
			$excerpt .= movedo_grve_read_more();
		} else{
			$excerpt = '<p>' . wp_trim_words( $content, $limit ) . '</p>';
		}
	}
	return	$excerpt;
}

function movedo_grve_quote_excerpt( $limit ) {
	global $post;
	$post_id = $post->ID;

	$content = movedo_grve_post_meta( '_movedo_grve_post_quote_text' );
	if( is_single() ){
		$excerpt = '<p>' . wp_kses_post( $content ) . '</p>';
	} else {
		$excerpt = '<p>' . wp_trim_words( $content, $limit ) . '</p>';
	}

	return	$excerpt;
}

 /**
 * Custom more
 */
function movedo_grve_read_more() {
	$more_button = '<a class="grve-read-more grve-link-text grve-heading-color grve-text-hover-primary-1" href="' . esc_url( get_permalink( get_the_ID() ) ) . '"><span>' . esc_html__( 'read more', 'movedo' ) . '</span></a>';
    return $more_button;
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
