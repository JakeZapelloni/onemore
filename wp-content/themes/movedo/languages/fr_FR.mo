��    ,      |  ;   �      �     �     �  
   �     �     �     �     �          #     8     I     N     U     d     j     x     �     �     �     �  	   �     �     �     �     �  
             %     4  O   C  E   �     �     �     �     �            $   #     H     K  	   N  	   X     b  �  u     g	     w	     �	     �	     �	     �	     �	     �	     �	     �	  	   

     
     
     6
     =
     R
     j
     z
     �
     �
  	   �
     �
     �
  $   �
     �
               1     =  O   U  h   �       
        "     +     @     Q  /   j     �     �  	   �  	   �     �                      "                )                 (              !                               $   *   +                                      '      	         &             
   ,       %      #        %s (Invalid) %s (Pending) (required) All Archives By: CSS Classes (optional) Cancel Reply Comments are closed. Daily Archives : Days E-mail Edit Menu Item Hours Leave a Reply Leave a Reply to Log out Log out of this account Logged in as Monthly Archives : Move down Move up Name Open link in a new window/tab Phone: Posts By : Search Results for : Search for ... Submit Comment The description will be displayed in the menu if the current theme supports it. This post is password protected. Enter the password to view comments. Website Website: Weeks Yearly Archives : You must be Your Comment Here... Your comment is awaiting moderation. at in logged in read more to post a comment. Project-Id-Version: Movedo v1.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-01-23 10:53+0200
PO-Revision-Date: 2017-01-23 10:53+0200
Last-Translator: 
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n>1;
X-Generator: Poedit 1.8.5
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: woocommerce
X-Poedit-SearchPathExcluded-1: includes/framework
 %s (non valide) %s (en attente) (requis) Tous Archives Par : Classes CSS (facultatif) Annuler une réponse Les commentaires sont fermés. Archives journalières : Journées E-mail Modifier élément du menu Heures Laisser une réponse Laisser une réponse à Se déconnecter Se déconnecter du compte Connecté comme Archives mensuelles : Descendre Monter Nom Ouvrir le lien dans un nouvel onglet Téléphone : Articles par : Résultats de recherches pour : Cherchez... Sumettre un commentaire La description sera affichée dans le menu si le thème actuel l&rsquo;accepte. Ce poste est protégé par mot de passe. Veuillez entrer le mot de passe pour accéder aux commentaires. Site Web Site Web : Semaines Archives annuelles : Vous devez être Votre Commentaire Ici... Votre commentaire est en attente de moderation. à dans connecté lire plus pour poster un commentaire. 